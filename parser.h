#ifndef PARSER
#define PARSER

#include <string>
#include <vector>

enum class ParameterType
{
    number, string
};

struct Parameter
{
    std::string mName;
    ParameterType mType;
    double mValueNumber;
    std::string mValueString;

    Parameter(std::string nName, double nValue) :
        mName{nName}
      , mType{ParameterType::number}
      , mValueNumber{nValue}
      , mValueString{""}
    {

    }

    Parameter(std::string nName, std::string nValue) :
        mName{nName}
      , mType{ParameterType::string}
      , mValueNumber{0.0}
      , mValueString{nValue}
    {

    }

    void operator =(const Parameter& other)
    {
        mName = other.mName;
        mType = other.mType;
        mValueNumber = other.mValueNumber;
        mValueString = other.mValueString;
    }
};

std::string separate(std::string& data, int position, std::string separator);
std::vector<Parameter> identifyParameters(std::string nParameterList);

#endif // PARSER

