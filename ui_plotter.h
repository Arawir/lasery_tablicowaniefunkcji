#ifndef UI_PLOTTER
#define UI_PLOTTER

#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>
#include <QTextEdit>
#include <QFrame>
#include <QCheckBox>
#include <QPushButton>
#include <QChartView>
#include <QLineSeries>

#include "ui_engine.h"

using namespace QtCharts;

namespace Ui{

    class Plotter : public QFrame
    {
        Q_OBJECT
    private:
        QVBoxLayout * mLayout1;
        QHBoxLayout * mLayout2;
        QTextEdit *mXMin;
        QTextEdit *mXMax;
        QTextEdit *mYMin;
        QTextEdit *mYMax;
        QPushButton *mAutoscale;
        QPushButton * mPlot;

        QChart *mChart;
        QChartView *mChartView;

        iMainLayout& mMainBox;

    public:
        explicit Plotter(iMainLayout& nMainBox) :
            QFrame(0)
          , mMainBox{nMainBox}
        {
            mLayout1 = new QVBoxLayout();
            setLayout(mLayout1);

            mLayout2 = new QHBoxLayout();
            mLayout1->addLayout(mLayout2);

            mXMin = new QTextEdit{};
            mXMin->setPlaceholderText("x min");
            mXMin->setMaximumHeight(30);
            mLayout2->addWidget(mXMin);

            mXMax = new QTextEdit{};
            mXMax->setPlaceholderText("x max");
            mXMax->setMaximumHeight(30);
            mLayout2->addWidget(mXMax);

            mYMin = new QTextEdit{};
            mYMin->setPlaceholderText("y min");
            mYMin->setMaximumHeight(30);
            mLayout2->addWidget(mYMin);

            mYMax = new QTextEdit{};
            mYMax->setPlaceholderText("y max");
            mYMax->setMaximumHeight(30);
            mLayout2->addWidget(mYMax);

            mAutoscale = new QPushButton("Autoscale");
            connect(mAutoscale, SIGNAL(pressed()), this, SLOT(autoscale()));
            mLayout2->addWidget(mAutoscale);


            mPlot = new QPushButton("Plot");
            connect(mPlot, SIGNAL(pressed()), this, SLOT(replot()));
            mLayout1->addWidget(mPlot);

            mChart = new QChart();
            mChart->legend()->hide();
            //mChart->addSeries(series);
            mChart->createDefaultAxes();
           // mChart->setTitle("Simple line chart example");

            mChartView = new QChartView(mChart);
            mChartView->setRenderHint(QPainter::Antialiasing);
            mLayout1->addWidget(mChartView);

            setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

        }

        double xMax()
        {
            return mXMax->toPlainText().toDouble();
        }

        double xMin()
        {
            return mXMin->toPlainText().toDouble();
        }

        double yMax()
        {
            return mYMax->toPlainText().toDouble();
        }

        double yMin()
        {
            return mYMin->toPlainText().toDouble();
        }

    public slots:
        void replot()
        {
            mMainBox.prepareToReplot();
            clearChart();

            if(!mMainBox.lineSeries().empty()){
                for(auto& dataSeries : mMainBox.lineSeries()){
                    mChart->addSeries(dataSeries);
                }
                configureChart();
            }
        }

        void autoscale()
        {
            qreal minY = 0.0;
            qreal maxY = 0.0;
            qreal newY = 0.0;

            if(!mMainBox.lineSeries().empty()){
                minY = mMainBox.lineSeries().first()->at(0).y();
                maxY = mMainBox.lineSeries().first()->at(0).y();

                for(auto& dataSeries : mMainBox.lineSeries()){
                    for(auto& point : dataSeries->points()){
                        newY = point.ry();
                        if(newY<minY){ minY = newY; }
                        if(newY>maxY){ maxY = newY; }
                    }
                }
                mYMin->setText(QString::number(minY));
                mYMax->setText(QString::number(maxY));
                configureChart();
            }

        }

    private:
        void clearChart()
        {
            for(auto& series : mChart->series()){
                mChart->removeSeries(series);
            }
        }

        void configureChart()
        {
            mChart->createDefaultAxes();

            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
            mChart->axisX()->setMax(xMax());
            mChart->axisX()->setMin(xMin());
            mChart->axisY()->setMax(yMax());
            mChart->axisY()->setMin(yMin());
            #pragma GCC diagnostic pop
        }
    };

}
#endif // UI_PLOTTER

