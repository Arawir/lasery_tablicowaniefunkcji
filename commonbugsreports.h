#ifndef COMMONBUGSREPORTS
#define COMMONBUGSREPORTS
#include <iostream>
enum class Raport
{
   ok
    , wrongFunctionType
    , wrongParameterName
    , wrongParameterValue
    , wrongParameterType
    , functionNameIsAlredyInUse
    , wrongFunctionName
};

#endif // COMMONBUGSREPORTS

