#include "parser.h"
#include "assert.h"
#include <iostream>

std::string separate(std::string& data, int position, std::string separator)
{
    std::string output = data.substr(position, data.find(separator));
    data.erase(position, data.find(separator)+1);
    return output;
}

std::string removeSpaces(std::string nText)
{
    std::string oText = "";

    for(uint i=0; i<nText.length(); i++)
    {
        if(nText.at(i)!=' '){
            oText += nText.at(i);
        }
    }

    return oText;
}

std::vector<Parameter> identifyParameters(std::string nParameterList)
{
    //remove spaces
    std::vector<Parameter> oParameterList;
    std::string parameterText;
    std::string nameText;
    std::string valueText;

    std::string parameterList = removeSpaces(nParameterList);

    while(!parameterList.empty()){
        if(parameterList.find(";") == std::string::npos){
            parameterText = parameterList;
            parameterList.clear();
        } else {
            parameterText = separate(parameterList, 0, ";");
        }

        nameText = separate(parameterText, 0, "=");
        valueText = parameterText;

        if( (valueText[0] == '\"') ){
            valueText.erase(0,1);
            valueText.erase(valueText.size()-1,1);
            //std::cout << valueText << std::endl;
            oParameterList.push_back( {nameText, valueText });
        } else {
            oParameterList.push_back( {nameText, std::stod(valueText) });
        }
    }

    return oParameterList;
}
