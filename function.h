#ifndef FUNCTION_H
#define FUNCTION_H

#include <vector>
#include <fstream>
#include <algorithm>
#include "ifunction.h"
#include "parser.h"
#include "basicmath.h"
#include "ifbox.h"

struct Point
{
    double x;
    double value;

    Point(double newX, double newValue) :
        x{newX}
      , value{newValue}
    {

    }
};

class Function : public iFunction
{
protected:
    iFBox* mBox;
private:
    std::vector<Point> points;
    std::vector<Parameter> mParameters;
    std::string mName;

public:
    Function(std::string nName, iFBox& nBox) :
        iFunction{}
      , mBox{&nBox}
      , points{}
      , mName{nName}
    {

    }

    ~Function() = default;

    virtual double valueN(double x) override
    {
        if(!isStored(x)){
            addPoint(x);
        }
        return valueInternal(x);
    }

    virtual std::string name() override
    {
        return mName;
    }

    virtual void saveToFile() override
    {
        std::fstream file(mName + ".data", std::ios::out);

        for(auto& point : points){
            file << point.x << " " << point.value << std::endl;
        }

        file.close();
    }

    virtual double operator()(double x) override
    {
        return valueN(x);
    }

    virtual Raport matchParameters(std::vector<Parameter> nParameters) override
    {
        for(auto& parameter : nParameters)
        {
            Parameter* lParameter = getParameter(parameter.mName);
            if(lParameter==nullptr){ return Raport::wrongParameterName; }
            if(lParameter->mType != parameter.mType){ return Raport::wrongParameterType; }
            *lParameter = parameter;
        }
        return Raport::ok;
    }

    virtual Raport update(std::vector<Parameter> nParameters) override
    {
        points.clear();
        return matchParameters(nParameters);
    }

    Parameter* getParameter(std::string parameterName) override
    {
        for(auto& parameter : mParameters){
            if(parameter.mName==parameterName){ return &parameter; }
        }
        return nullptr;
    }

protected:
    void addParameter( Parameter nParameter)
    {
        mParameters.push_back(nParameter);
    }



    double valueN(std::string parameterName)
    {
        return getParameter(parameterName)->mValueNumber;
    }


    std::string valueS(std::string parameterName)
    {
        return getParameter(parameterName)->mValueString;
    }

private:
    void addPoint(double x)
    {
        points.push_back( {x, formula(x)});
    }

    bool isStored(double x)
    {
        for(auto& point : points){
            if(point.x == x){ return true; }
        }
        return false;
    }

    double valueInternal(double x)
    {
        for(auto& point : points){
            if(point.x == x){ return point.value; }
        }
        assert(false && "cannot find point with position x");
        return 0.0;
    }

};

#endif // FUNCTION_H
