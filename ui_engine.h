#ifndef UI_ENGINE
#define UI_ENGINE

#include <QLineSeries>
#include <QList>
#include <QString>

#include "commonbugsreports.h"

using namespace QtCharts;

namespace Ui{

    class iMainLayout
    {
        //Q_OBJECT
    public:
        iMainLayout(){ }
        ~iMainLayout() = default;

        virtual QList<QLineSeries*> lineSeries() = 0;
        virtual Raport addFunction(QString type, QString name, QString parameters) = 0;
        virtual Raport updateFunction(QString name, QString parameters) = 0;
        virtual void removeFunction(QString name) = 0;
        virtual void prepareToReplot() = 0;
        virtual double functionValue(QString name, double x) = 0;
        virtual void saveFunction(QString name) = 0;

        virtual double xMax() = 0;
        virtual double xMin() = 0;

    };
}

#endif // UI_ENGINE

