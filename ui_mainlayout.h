#ifndef UI_MAINLAYOUT
#define UI_MAINLAYOUT

#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QLabel>
#include <QMessageBox>

#include "ui_function.h"
#include "ui_plotter.h"
#include "ui_engine.h"
#include <QList>
#include <QString>

#include "ifbox.h"

namespace Ui{

    class MainLayout : public QWidget, public iMainLayout
    {
        Q_OBJECT
    private:
        QHBoxLayout* mLayout1;
        QVBoxLayout* mLayout2;
        QVBoxLayout* mLayout3;

        QLabel * mLabel1;
        QLabel * mLabel2;

        QList<Ui::Function*> mFunctions;
        Ui::Plotter *mPlotter;

        QPushButton* mAddFunctionButton;

        QScrollArea *mFunctionsScrollArea;
        QWidget* tmp;

        iFBox& mFBox;

    public:
        explicit MainLayout(iFBox& nFBox) :
           QWidget(0)
         , mFBox{nFBox}
        {
            mLayout1 = new QHBoxLayout();
            setLayout(mLayout1);

            mLayout2 = new QVBoxLayout();
            mLayout2->setAlignment(Qt::AlignTop);

            tmp = new QWidget{};
            tmp->setLayout(mLayout2);

            mFunctionsScrollArea = new QScrollArea{};
            mFunctionsScrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);

            mFunctionsScrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
            mFunctionsScrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );
            mFunctionsScrollArea->setWidgetResizable( true );
            mFunctionsScrollArea->setWidget( tmp );

            mFunctionsScrollArea->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
            mLayout1->addWidget(mFunctionsScrollArea);


            mLayout3 = new QVBoxLayout();
            mLayout1->addLayout(mLayout3);

            mLabel1 = new QLabel("Functions");
            mLabel1->setAlignment(Qt::AlignHCenter);
            mLayout2->addWidget(mLabel1);

            mLabel2 = new QLabel("Chart options");
            mLabel2->setAlignment(Qt::AlignHCenter);
           // mLayout3->addWidget(mLabel2);

            mAddFunctionButton = new QPushButton("Add new function");
            connect(mAddFunctionButton, SIGNAL(pressed()), this, SLOT(addFunction()));
            mLayout2->addWidget(mAddFunctionButton);

            addFunction();




            mPlotter = new Ui::Plotter{*this};
            mLayout3->addWidget(mPlotter);
        }

        QList<QLineSeries*> lineSeries() override
        {
            QList<QLineSeries*> outputList;

            for(auto& function : mFunctions){
                if(function->needToBePlotted()){
                    outputList.push_back(function->dataSeries());
                }
            }
            return outputList;
        }


        Raport addFunction(QString type, QString name, QString parameters)
        {
            Raport result = mFBox.addFunction(type.toStdString(), name.toStdString(), parameters.toStdString());
            raportDialog(result);
            return result;
        }

        Raport updateFunction(QString name, QString parameters)
        {
            Raport result = mFBox.updateFunction(name.toStdString(), parameters.toStdString());
            raportDialog(result);
            return result;
        }

        void removeFunction(QString name)
        {
            mFBox.removeFunction(name.toStdString());
        }

        void saveFunction(QString name)
        {
            mFBox.saveFunction(name.toStdString());
        }

        void prepareToReplot() override
        {
            for(auto& function : mFunctions){
                function->updateDataSeries();
            }
        }

        double functionValue(QString name, double x) override
        {
            return mFBox.functionValue(name.toStdString(), x);
        }

        double xMax()
        {
            return mPlotter->xMax();
        }

        double xMin()
        {
            return mPlotter->xMin();
        }


    public slots:
        void addFunction()
        {
            Ui::Function* function = new Ui::Function{*this};
            mFunctions.push_back(function);
            mLayout2->addWidget(function);
            function->show();
        }
    private:
        void raportDialog(Raport raportType)
        {
            QMessageBox msgBox;
            if(raportType != Raport::ok){
                switch (raportType){
                case Raport::wrongFunctionType :
                    msgBox.setText("Wrong function type");
                    break;
                case Raport::functionNameIsAlredyInUse :
                    msgBox.setText("Function name is already in use");
                    break;
                case Raport::wrongParameterName :
                    msgBox.setText("Wrong parameter(s) type");
                    break;
                default :
                    msgBox.setText("Unknown error");
                    break;
                }

                msgBox.exec();
            }
        }
    };

}
#endif // UI_MAINLAYOUT

