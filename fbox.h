#ifndef FBOX
#define FBOX

#include <vector>
#include "ifbox.h"
#include "assert.h"
#include "basicfunctions.h"
#include "laguerrefunctions.h"
#include "legendrefunctions.h"
#include "hermitefunctions.h"

class FBox : public iFBox
{
private:
    std::vector<iFunction*> functions;
public:
    FBox() :
        iFBox{}
    {
        addCommonFunctions();
    }

    ~FBox() = default; //check this leeks

    void addFunction(iFunction* newFunction) override
    {
        functions.push_back(newFunction);
    }

    iFunction* function(std::string functionName) override
    {
        for(auto& function : functions){
            if(function->name() == functionName){ return function; }
        }
        assert(false && ("Cannot find function with name "+functionName).c_str());
        return nullptr;
    }

    double functionValue(std::string functionName, double x)
    {
        return function(functionName)->valueN(x);
    }

    Raport addFunction(std::string type, std::string name, std::string parameters)
    {
        if(type == "const"){
            return addFunctionIfPossible( new ConstantFunction{name, *this}, parameters );
        }
        if(type == "lin"){
            return addFunctionIfPossible( new LinearFunction{name, *this}, parameters );
        }
        if(type == "mul"){
            return addFunctionIfPossible( new Multiplication{name, *this}, parameters );
        }
        if(type == "sin"){
            return addFunctionIfPossible( new Sinus{name, *this}, parameters );
        }
        if(type == "exp"){
            return addFunctionIfPossible( new Exponenta{name, *this}, parameters );
        }
        if(type == "pow"){
            return addFunctionIfPossible( new Power{name, *this}, parameters );
        }
        if(type == "der"){
            return addFunctionIfPossible( new Derivative{name, *this}, parameters );
        }
        if(type == "int"){
            return addFunctionIfPossible( new Integral{name, *this}, parameters );
        }
        if( (type=="lag") | (type=="laguerre") ){
            return addFunctionIfPossible( new Laguerre{name, *this}, parameters );
        }
        if( (type=="lag_a") | (type=="laguerre_a") ){
            return addFunctionIfPossible( new LaguerreAnalytical{name, *this}, parameters );
        }
        if( (type=="lag_int") | (type=="laguerre_integral") ){
            return addFunctionIfPossible( new LaguerreIntegral{name, *this}, parameters );
        }
        if( (type=="leg") | (type=="legandrere") ){
            return addFunctionIfPossible( new Legandre{name, *this}, parameters );
        }
        if( (type=="leg_int") | (type=="legandrere_integral") ){
            return addFunctionIfPossible( new LegandreIntegral{name, *this}, parameters );
        }
        if( (type=="her") | (type=="hermite") ){
            return addFunctionIfPossible( new Hermite{name, *this}, parameters );
        }
        if( (type=="her_int") | (type=="hermite_integral") ){
            return addFunctionIfPossible( new HermiteIntegral{name, *this}, parameters );
        }
        return Raport::wrongFunctionType;
    }

    Raport updateFunction(std::string name, std::string parameters)
    {
        iFunction* lFunction = function(name);
        if( lFunction == nullptr ){ return Raport::wrongFunctionName; }
        return lFunction->update(identifyParameters(parameters));
    }

    void removeFunction(std::string name)
    {
        for(uint i=0; i<functions.size(); i++){
            if(name==functions[i]->name()){ functions.erase(functions.begin()+i); }
        }
        assert(false && "erase function fail");
    }

    void saveFunction(std::string functionName) override
    {
        function(functionName)->saveToFile();
    }

private:
    Raport addFunctionIfPossible(iFunction* nFunction, std::string parameters)
    {
        if(existsFunctionWithName(nFunction->name())){ return Raport::functionNameIsAlredyInUse; }
        Raport result = nFunction->matchParameters(identifyParameters(parameters));
        if(result != Raport::ok){
            delete nFunction;
        } else {
            functions.push_back( nFunction );
        }
        return result;
    }

    bool existsFunctionWithName(std::string functionName)
    {
        for(auto& function : functions){
            if(function->name() == functionName){ return true; }
        }
        return false;
    }

    void addCommonFunctions()
    {
        std::string parameters = "A=1.0;B=0.0;";
        addFunctionIfPossible( new LinearFunction("x",*this), parameters);

        parameters = "Value=1.0";
        addFunctionIfPossible( new ConstantFunction("1.0",*this), parameters);
    }
};

#endif // FBOX

