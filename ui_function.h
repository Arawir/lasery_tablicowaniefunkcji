#ifndef UI_FUNCTION
#define UI_FUNCTION

#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>
#include <QTextEdit>
#include <QFrame>
#include <QCheckBox>
#include <QPushButton>
#include <QLineSeries>

#include "ui_engine.h"

using namespace QtCharts;

namespace Ui{

    class Function : public QFrame
    {
        Q_OBJECT
    private:
        QVBoxLayout * mLayout1;
        QHBoxLayout * mLayout2;
        QHBoxLayout * mLayout3;
        QTextEdit *mType;
        QTextEdit *mName;
        QTextEdit *mParameters;

        QCheckBox * mToPlot;
        QPushButton * mUpdate;
        QPushButton * mRemove;
        QPushButton * mSave;

        QLineSeries* mDataSeries;
        iMainLayout& mMainBox;

        bool mIsAddedCorrectly;
    public:
        explicit Function(iMainLayout& nMainBox) :
            QFrame(0)
          , mMainBox{nMainBox}
          , mIsAddedCorrectly{false}
        {
            setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

            mLayout1 = new QVBoxLayout();
            setLayout(mLayout1);

            mLayout2 = new QHBoxLayout();
            mLayout1->addLayout(mLayout2);

            mType = new QTextEdit{};
            mType->setPlaceholderText("funtion type");
            mType->setMaximumHeight(30);
            mLayout2->addWidget(mType);

            mName = new QTextEdit{};
            mName->setPlaceholderText("funtion name");
            mName->setMaximumHeight(30);
            mLayout2->addWidget(mName);

            mParameters = new QTextEdit{};
            mParameters->setPlaceholderText("parameter list");
            mParameters->setMaximumHeight(30);
            mLayout1->addWidget(mParameters);

            mLayout3 = new QHBoxLayout();
            mLayout1->addLayout(mLayout3);

            mToPlot = new QCheckBox("Plot");
            mLayout3->addWidget(mToPlot);

            mSave = new QPushButton("Save");
            connect(mSave, SIGNAL(pressed()), this, SLOT(saveToFile()));
            mLayout3->addWidget(mSave);

            mUpdate = new QPushButton("Add");
            connect(mUpdate, SIGNAL(pressed()), this, SLOT(update()));
            mLayout3->addWidget(mUpdate);

            mRemove = new QPushButton("Remove");
            connect(mRemove, SIGNAL(pressed()), this, SLOT(remove()));
           // mLayout3->addWidget(mRemove); //temporary unlocked

            mDataSeries = new QLineSeries;

            setFrameStyle(QFrame::StyledPanel | QFrame::Plain);


        }

        QLineSeries* dataSeries()
        {
            return mDataSeries;
        }

        bool needToBePlotted()
        {
            return mToPlot->isChecked();
        }

        void updateDataSeries()
        {
            if(mIsAddedCorrectly){
                mDataSeries->clear();
                double xDelta = 0.1;
                double x = mMainBox.xMin();

                while(x<(mMainBox.xMax()+xDelta/2.0)){
                    double functionValue = mMainBox.functionValue(mName->toPlainText(), x);
                    mDataSeries->append(x, functionValue);
                    x+=xDelta;
                }
            }
        }

    public slots:
        void update()
        {
            if(mIsAddedCorrectly==false){
                Raport result = mMainBox.addFunction(mType->toPlainText(), mName->toPlainText(), mParameters->toPlainText());
                if(result == Raport::ok){
                    mIsAddedCorrectly = true;
                    mUpdate->setText("Update");
                    mType->setReadOnly(true);
                    mName->setReadOnly(true);
                }

            } else {
                Raport result = mMainBox.updateFunction(mName->toPlainText(), mParameters->toPlainText());
                if(result != Raport::ok){
                    mUpdate->setText("Retry update");
                }
            }
        }

        void remove()
        {
            mMainBox.removeFunction(mName->toPlainText());
        }

        void saveToFile()
        {
            mMainBox.saveFunction(mName->toPlainText());
            //TODO add raport
        }

    };

}



#endif // UI_FUNCTION

