#ifndef LAGUERREFUNCTIONS
#define LAGUERREFUNCTIONS

#include "basicfunctions.h"

class LaguerreInternalFunction : public Function
{
public:
    LaguerreInternalFunction(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter( {"alpha", 1.0});
        addParameter( {"n", 1.0});
    }

    double formula(double x) override
    {
        return exp(-x)* pow(x,valueN("n")+valueN("alpha"));
    }
};

class Laguerre : public Function
{
    LaguerreInternalFunction mInternamFunction;

public:
    Laguerre(std::string nName, iFBox& nBox) :
        Function(nName, nBox)
      , mInternamFunction("Internal", nBox)
    {
        addParameter( {"alpha", 1.0});
        addParameter( {"n", 1.0});
        addParameter( {"derivativeDelta", 0.1});
    }

    Raport matchParameters(std::vector<Parameter> nParameters) override
    {
        Raport result = Function::matchParameters(nParameters);
        if(result!=Raport::ok) return result;
        return mInternamFunction.matchParameters(nParameters);
    }

    Raport update(std::vector<Parameter> nParameters) override
    {
        Raport result = Function::update(nParameters);
        if(result!=Raport::ok) return result;
        return mInternamFunction.update(nParameters);
    }

    double formula(double x) override
    {
        if(x<=0.0) return 0.0;
        return exp(x) * pow(x,-valueN("alpha")) / factorial(valueN("n")) * derivative(mInternamFunction,x,valueN("n"),valueN("derivativeDelta"));
    }
};

class LaguerreAnalytical : public Function
{
public:
    LaguerreAnalytical(std::string nName, iFBox& nBox) :
        Function(nName, nBox)
    {
        addParameter( {"alpha", 1.0});
        addParameter( {"n", 1.0});
    }

    double formula(double x) override
    {
        if(x<=0.0) return 0.0;
        return analitycalLaguerre(valueN("n"), valueN("alpha"), x);
    }
};


class LaguerreIntegral : public Function
{
public:
    LaguerreIntegral(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"L1", "0.0"});
        addParameter({"L2", "0.0"});
        addParameter({"xi", 0.0});
        addParameter({"deltaX", 0.1});
    }

    double formula(double x) override
    {
        iFunction* laguerre1 = mBox->function(valueS("L1"));
        iFunction* laguerre2 = mBox->function(valueS("L2"));
        return integralLaguerre(*laguerre1, *laguerre2, valueN("xi"), x, valueN("deltaX"));
    }

};

#endif // LAGUERREFUNCTIONS

