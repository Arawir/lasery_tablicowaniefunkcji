#-------------------------------------------------
#
# Project created by QtCreator 2020-03-03T17:45:46
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TablicowanieFunkcji
TEMPLATE = app
#LIBS += -armadillo -lapack -blas

SOURCES += main.cpp\
        mainwindow.cpp \
    parser.cpp \
    basicmath.cpp

HEADERS  += mainwindow.h \
    function.h \
    ifunction.h \
    laugerrepolinominal.h \
    ifbox.h \
    fbox.h \
    ui_function.h \
    ui_mainlayout.h \
    ui_plotter.h \
    ui_engine.h \
    parser.h \
    basicfunctions.h \
    basicmath.h \
    commonbugsreports.h \
    laguerrefunctions.h \
    legendrefunctions.h \
    hermitefunctions.h

FORMS    += mainwindow.ui

unix|win32: LIBS += -llapack -lblas -larmadillo
