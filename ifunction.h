#ifndef IFUNCTION_H
#define IFUNCTION_H

#include <string>
#include "assert.h"
#include "commonbugsreports.h"
#include "parser.h"

typedef unsigned int uint;

class iFunction
{
public:
    iFunction(){ }
    virtual ~iFunction() = default;

    virtual double valueN(double x) = 0;
    virtual double formula(double x) = 0;
    virtual Raport update(std::vector<Parameter> nParameters) = 0;
    virtual std::string name() = 0;
    virtual void saveToFile() = 0;
    virtual Raport matchParameters(std::vector<Parameter> nParameters) = 0;

    virtual Parameter* getParameter(std::string parameterName) = 0;
    virtual double operator()(double x) = 0;
};



#endif // IFUNCTION_H
