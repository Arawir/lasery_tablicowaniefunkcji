#ifndef LEGENDREFUNCTIONS
#define LEGENDREFUNCTIONS

#include "basicfunctions.h"


class LegandreInternalFUnction : public Function
{
public:
    LegandreInternalFUnction(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter( {"n", 1.0});
    }

    double formula(double x) override
    {
        return pow(x*x-1,valueN("n"));
    }
};

class Legandre : public Function
{
    LegandreInternalFUnction mInternamFunction;

public:
    Legandre(std::string nName, iFBox& nBox) :
        Function(nName, nBox)
      , mInternamFunction("Internal", nBox)
    {
        addParameter( {"n", 1.0});
        addParameter( {"derivativeDelta", 0.1});
    }

    Raport matchParameters(std::vector<Parameter> nParameters) override
    {
        Raport result = Function::matchParameters(nParameters);
        if(result!=Raport::ok) return result;
        return mInternamFunction.matchParameters(nParameters);
    }

    Raport update(std::vector<Parameter> nParameters) override
    {
        Raport result = Function::update(nParameters);
        if(result!=Raport::ok) return result;
        return mInternamFunction.update(nParameters);
    }

    double formula(double x) override
    {
        double n = valueN("n");
        double a = 1.0/pow(2.0,n)/factorial(n);
        return a*derivative(mInternamFunction,x,n,valueN("derivativeDelta"));
    }
};

class LegandreIntegral : public Function
{
public:
    LegandreIntegral(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"L1", "0.0"});
        addParameter({"L2", "0.0"});
        addParameter({"xi", 0.0});
        addParameter({"deltaX", 0.1});
    }

    double formula(double x) override
    {
        iFunction* legandre1 = mBox->function(valueS("L1"));
        iFunction* legandre2 = mBox->function(valueS("L2"));
        return integralLegandre(*legandre1, *legandre2, valueN("xi"), x, valueN("deltaX"));
    }

};

#endif // LEGENDREFUNCTIONS

