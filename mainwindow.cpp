#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mFBox = new FBox{};
    mMainLayout = new Ui::MainLayout{*mFBox};


    setCentralWidget(mMainLayout);
}

MainWindow::~MainWindow()
{
    delete ui;
}
