#ifndef HERMITEFUNCTIONS
#define HERMITEFUNCTIONS

#include "basicfunctions.h"


class HermiteInternalFUnction : public Function
{
public:
    HermiteInternalFUnction(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {

    }

    double formula(double x) override
    {
        return exp(-x*x);
    }
};

class Hermite : public Function
{
    HermiteInternalFUnction mInternamFunction;

public:
    Hermite(std::string nName, iFBox& nBox) :
        Function(nName, nBox)
      , mInternamFunction("Internal", nBox)
    {
        addParameter( {"n", 1.0});
        addParameter( {"derivativeDelta", 0.1});
    }

    double formula(double x) override
    {
        double n = valueN("n");
        double a = pow(-1.0,n)*exp(x*x);
        return a*derivative(mInternamFunction,x,n,valueN("derivativeDelta"));
    }
};

class HermiteIntegral : public Function
{
public:
    HermiteIntegral(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"H1", "0.0"});
        addParameter({"H2", "0.0"});
        addParameter({"xi", -10.0});
        addParameter({"deltaX", 0.1});
    }

    double formula(double x) override
    {
        iFunction* hermite1 = mBox->function(valueS("H1"));
        iFunction* hermite2 = mBox->function(valueS("H2"));
        return integralHermite(*hermite1, *hermite2, valueN("xi"), x, valueN("deltaX"));
    }

};

#endif // HERMITEFUNCTIONS

