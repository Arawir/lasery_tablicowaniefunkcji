#include "basicmath.h"
#include <armadillo>


double derivative(iFunction& function, double x, uint degree, double delta)
{
    uint N = ((degree+1)%2) ? degree+1 : degree+2;

    arma::mat A(N,N,arma::fill::zeros);
    arma::vec V(N, arma::fill::zeros);
    V[degree]=1.0;


    for(uint i=0; i<N; i++){
        for(uint j=0; j<N; j++){
            double n = static_cast<double>(j)-floor( static_cast<double>(N/2.0) );
            double p = static_cast<double>(i);
            A(i,j)= pow(n*delta,p)/factorial(p);
        }
    }
    V = A.i()*V;
    //std::cout << V << std::endl;
    double output = 0.0;
    for(int i=0; i<static_cast<int>(N); i++){
        double iDelta = delta*static_cast<double>(i) - delta*static_cast<double>(N/2);
        output += V[i]*function(x+iDelta);
    }

    return output;
}

double integral(iFunction &function, double xi, double xf, double delta)
{
    double x = xi;
    double oValue = 0.0;

    if(xi>=xf) return 0.0;

    while(x<(xf+delta/2.0)){
        oValue += function(x)*delta;
        x+=delta;
    }
    return oValue;
}

double integralLaguerre(iFunction &laguerre1, iFunction &laguerre2, double xi, double xf, double delta)
{
    double x = xi;
    double oValue = 0.0;
    double p=0.0;
    double alpha = laguerre1.getParameter("alpha")->mValueNumber;

    if(xi>=xf) return 0.0;

    while(x<(xf+delta/2.0)){
        p = pow(x,alpha)*exp(-x);
        oValue += p*laguerre1(x)*laguerre2(x)*delta;
        x+=delta;
    }
    return oValue;
}

double integralLegandre(iFunction &legandre1, iFunction &legandre2, double xi, double xf, double delta)
{
    double x = xi;
    double oValue = 0.0;

    if(xi>=xf) return 0.0;

    while(x<(xf+delta/2.0)){
        oValue += legandre1(x)*legandre2(x)*delta;
        x+=delta;
    }
    return oValue;
}

double integralHermite(iFunction &hermite1, iFunction &hermite2, double xi, double xf, double delta)
{
    double x = xi;
    double oValue = 0.0;
    double p=0.0;

    if(xi>=xf) return 0.0;

    while(x<(xf+delta/2.0)){
        p = exp(-x*x);
        oValue += p*hermite1(x)*hermite2(x)*delta;
        x+=delta;
    }
    return oValue;
}

double factorial(double n)
{
    if(n==0.0){ return 1.0; }
    if(n==1.0){ return 1.0; }
    return factorial(n-1.0)*n;
}



double analitycalLaguerre(int n, double alpha, double x)
{
    return exp(x)*pow(x,-alpha)/factorial(static_cast<double>(n))*pochLaguerre(n,n,alpha,x);
}

double pochLaguerre(int n, int p, double alpha, double x)
{
    if(n==0){ return exp(-x)*pow(x,p+alpha); }
    return (p+alpha)*pochLaguerre(n-1,p-1,alpha,x) - pochLaguerre(n-1,p,alpha,x);
}
