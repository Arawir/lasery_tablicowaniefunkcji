#ifndef LAUGERREPOLINOMINAL
#define LAUGERREPOLINOMINAL

#include "function.h"

class LaugerrePolinominal : public Function
{
private:
    double alpha;
    double n;

public:
    LaugerrePolinominal(double newAlpha, double newN) :
        Function()
      , alpha{newAlpha}
      , n{newN}
    {

    }

    double formula(double x) override
    {
        return exp(x)*pow(x,-alpha)/factorial(n);
    }

};

#endif // LAUGERREPOLINOMINAL

