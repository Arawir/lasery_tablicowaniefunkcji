#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "ui_mainlayout.h"
#include "fbox.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    FBox *mFBox;
    Ui::MainLayout* mMainLayout;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
