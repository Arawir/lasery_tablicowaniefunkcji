#ifndef IFBOX
#define IFBOX

#include "commonbugsreports.h"
#include "ifunction.h"

class iFBox
{
public:
    iFBox(){}
    ~iFBox() = default;

    virtual void addFunction(iFunction* newFunction) = 0;
    virtual iFunction* function(std::string functionName) = 0;

    virtual double functionValue(std::string functionName, double x) = 0;
    virtual Raport addFunction(std::string type, std::string name, std::string parameters) = 0;
    virtual Raport updateFunction(std::string name, std::string parameters) = 0;
    virtual void removeFunction(std::string name) = 0;
    virtual void saveFunction(std::string functionName) = 0;

};

#endif // IFBOX

