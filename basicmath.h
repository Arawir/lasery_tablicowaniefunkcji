#ifndef BASICMATH_H
#define BASICMATH_H

#include "ifunction.h"

double derivative(iFunction& function, double x, uint degree, double delta);
double integral(iFunction &function, double xi, double xf, double delta);
double factorial(double n);

double analitycalLaguerre(int n, double alpha, double x);
double pochLaguerre(int n, int p, double alpha, double x);

double integralLaguerre(iFunction &laguerre1, iFunction &laguerre2, double xi, double xf, double delta);
double integralLegandre(iFunction &legandre1, iFunction &legandre2, double xi, double xf, double delta);
double integralHermite(iFunction &hermite1, iFunction &hermite2, double xi, double xf, double delta);
#endif // BASICMATH_H
