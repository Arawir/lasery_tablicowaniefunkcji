#ifndef BASICFUNCTIONS
#define BASICFUNCTIONS

#include "function.h"

class ConstantFunction : public Function
{
public:
    ConstantFunction(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"Value", 1.0});
    }
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    double formula(double x) override
    {
        return valueN("Value");
    }
#pragma GCC diagnostic pop

};

class LinearFunction : public Function
{
public:
    LinearFunction(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"A", 1.0});
        addParameter({"B", 0.0});
    }
    double formula(double x) override
    {
        return valueN("A")*x+valueN("B");
    }
};

class Multiplication : public Function
{
public:
    Multiplication(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"A", "1.0"});
        addParameter({"B", "1.0"});
        addParameter({"C", "1.0"});
        addParameter({"D", "1.0"});
        addParameter({"E", "1.0"});
        addParameter({"F", "1.0"});

    }
    double formula(double x) override
    {
        double a = mBox->function(valueS("A"))->valueN(x) ;
        double b = mBox->function(valueS("B"))->valueN(x) ;
        double c = mBox->function(valueS("C"))->valueN(x) ;
        double d = mBox->function(valueS("D"))->valueN(x) ;
        double e = mBox->function(valueS("E"))->valueN(x) ;
        double f = mBox->function(valueS("F"))->valueN(x) ;

        return a*b*c*d*e*f;
    }
};

class Sinus : public Function
{
public:
    Sinus(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"A", 1.0});
        addParameter({"W", 1.0});
        addParameter({"F", 0.0});
    }
    double formula(double x) override
    {
        return valueN("A") * sin( valueN("W")*x + valueN("F") );
    }
};



class Power : public Function
{
public:
    Power(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"A", 1.0});
        addParameter({"B", 1.0});
        addParameter({"C", 0.0});
    }
    double formula(double x) override
    {
        return valueN("A") * pow(x, valueN("B")) + valueN("C");
    }
};



class Exponenta : public Function
{
public:
    Exponenta(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"A", 1.0});
        addParameter({"B", 1.0});
        addParameter({"C", 0.0});
    }
    double formula(double x) override
    {
        return valueN("A") * exp( valueN("B")*x + valueN("C") );
    }
};



class Derivative : public Function
{
public:
    Derivative(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"n", 1.0});
        addParameter({"delta", 0.1});
        addParameter({"f", "1.0"});
    }

    double formula(double x) override
    {
        return derivative(*(mBox->function(valueS("f"))),x,valueN("n"),valueN("delta"));
    }

};


class Integral : public Function
{
public:
    Integral(std::string nName, iFBox& nBox) :
        Function{nName, nBox}
    {
        addParameter({"delta", 0.01});
        addParameter({"xi", 0.0});
        addParameter({"f", "1.0"});
    }

    double formula(double x) override
    {
        return integral(*(mBox->function(valueS("f"))),valueN("xi"),x,valueN("delta"));
    }

};

#endif // BASICFUNCTIONS

